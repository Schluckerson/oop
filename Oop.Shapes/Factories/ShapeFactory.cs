﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
			if (radius <= 0) throw new ArgumentOutOfRangeException("radius");
			Shape temp = new Circle(radius);
			return temp;
		}

		public Shape CreateTriangle(int a, int b, int c)
		{
			if ((a <= 0) || (b <= 0) || (c <= 0)) throw new ArgumentOutOfRangeException("a", "b", "c");
			if (a + b < c || a + c < b || b + c < a) throw new InvalidOperationException();
			Shape temp = new Triangle(a, b, c);
			return temp;
		}

		public Shape CreateSquare(int a)
		{
			if (a <= 0) throw new ArgumentOutOfRangeException("a");
			Shape temp = new Square(a);
			return temp;
		}

		public Shape CreateRectangle(int a, int b)
		{
			if ((a <= 0) || (b <= 0)) throw new ArgumentOutOfRangeException("a", "b");
			Shape temp = new Rectangle(a, b);
			return temp;
		}
	}
}