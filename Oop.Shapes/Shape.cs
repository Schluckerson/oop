﻿namespace Oop.Shapes
{
	public abstract class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public double Area { get; set; }

		/// <summary>
		/// Периметр
		/// </summary>
		public double Perimeter { get; set; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public int VertexCount { get; set; }

		public virtual bool IsEqual(Shape shape) => false;
	}

	public class Circle : Shape
    {
		public int radius { get; }
		public Circle(int radius)
        {
			this.radius = radius;
			Area = (radius * radius) * System.Math.PI;
			Perimeter = 2 * System.Math.PI * radius;	
			VertexCount = 0;
        }
		public override bool IsEqual(Shape shape)
		{
			if (!(shape is Circle)) return false;
			Circle circle = shape as Circle;
			if (radius == circle.radius) return true;
			else return false;
		}
	}

	public class Square : Shape
    {
		public int A { get; }

		public Square(int a)
        {
			A = a;
			VertexCount = 4;
			Perimeter = A * 4;
			Area = A * A;
        }
		public override bool IsEqual(Shape shape)
		{
			if (!(shape is Square)) return false;
			Square square = shape as Square;
			if (A == square.A) return true;
			else return false;
		}
	}

	public class Triangle : Shape
	{
		public int A { get;}
		public int B { get;}
		public int C { get;}
		public Triangle (int a, int b, int c)
        {
			A = a;
			B = b;
			C = c;
			Perimeter = A + B + C;
			double p = Perimeter / 2;
			double underSquare = p * (p - A) * (p - B) * (p - C);
			Area = System.Math.Sqrt(underSquare);
			VertexCount = 3;
		}
		public override bool IsEqual(Shape shape)
		{
			if (!(shape is Triangle)) return false;
			Triangle triangle = shape as Triangle;
			int counter = 0;
			int[] array = new int[] {triangle.A, triangle.B, triangle.C};
			foreach (int side in array)
            {
				if (side == A || side == B || side == C) counter++;
            }
			if (counter == 3) return true;
			else return false;
		}
	}

	public class Rectangle : Shape
    {
		public int A { get;}
		public int B { get;}

		public Rectangle(int a, int b)
        {
			A = a;
			B = b;
			Perimeter = (A + B) * 2;
			Area = A * B;
			VertexCount = 4;
        }
        public override bool IsEqual(Shape shape)
        {
			if (!(shape is Rectangle)) return false;
			Rectangle rectangle = shape as Rectangle;
			int counter = 0;
			int[] array = new int[] { rectangle.A, rectangle.B};
			foreach (int side in array)
			{
				if (side == A || side == B) counter++;
			}
			if (counter == 2) return true;
			else return false;
		}
    }
}
